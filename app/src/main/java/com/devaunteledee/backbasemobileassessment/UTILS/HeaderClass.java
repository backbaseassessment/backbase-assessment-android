package com.devaunteledee.backbasemobileassessment.UTILS;

/**
 * Created by devaunteledee on 3/11/16.
 */
public class HeaderClass implements Item {

    private final String title;

    public HeaderClass(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public boolean isSection() {
        return true;
    }
}