package com.devaunteledee.backbasemobileassessment.UTILS;

/**
 * Created by devaunteledee on 3/10/16.
 */
public class EmployeeCustomClass implements Item {
    String name;
    String surname;
    String email;
    String role;
    String photo;

    public EmployeeCustomClass(String name, String surname, String email, String role, String photo) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.role = role;
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @Override
    public boolean isSection() {
        return false;
    }
}
