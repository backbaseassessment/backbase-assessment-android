package com.devaunteledee.backbasemobileassessment.UTILS;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by devaunteledee on 3/10/16.
 */
public class EmployeeAsyncTask extends android.os.AsyncTask<String, Integer, String> {
    public Adapterhelp mUpdater;
    String name;
    String surname;
    String email;
    String photo;
    String role;
    String jsonString;
    ArrayList<EmployeeCustomClass> LaunchpadArrayList;
    ArrayList<EmployeeCustomClass> CXPArrayList;
    ArrayList<EmployeeCustomClass> MobileArrayList;
    private ProgressDialog progressDialog;
    private Context mContext;

    public EmployeeAsyncTask(Context context, Adapterhelp _Adapterhelp) {
        mContext = context;
        mUpdater = _Adapterhelp;

    }

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setProgressStyle((ProgressDialog.STYLE_SPINNER));
        progressDialog.setMessage("Loading Data from Backbase");
        progressDialog.setIndeterminate(true);
        progressDialog.setProgressPercentFormat(null);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            URL url = new URL(strings[0]);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            InputStream is = connection.getInputStream();
            jsonString = IOUtils.toString(is);
            is.close();
            connection.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }

    @Override
    protected void onPostExecute(String s) {

        super.onPostExecute(s);
        progressDialog.dismiss();
        try {
            LaunchpadArrayList = new ArrayList<>();
            CXPArrayList = new ArrayList<>();
            MobileArrayList = new ArrayList<>();

            //JSON
            JSONObject outerObject = new JSONObject(jsonString);
// Grabs the Launchpad json array
            JSONArray launchpadArray = outerObject.getJSONArray("Launchpad");
// loops through it and grabs all information
            for (int i = 0; i < launchpadArray.length(); i++) {

                JSONObject childObject = launchpadArray.getJSONObject(i);

                Log.i("OBJECT", "OBJECT........" + childObject.getString("name"));
                Log.i("OBJECT", "OBJECT........" + childObject.getString("surname"));
                Log.i("OBJECT", "OBJECT........" + childObject.getString("email"));
                Log.i("OBJECT", "OBJECT........" + childObject.getString("photo"));
                Log.i("OBJECT", "OBJECT........" + childObject.getString("role"));


                if (childObject.has("name")) {
                    name = childObject.getString("name");
                } else {
                    name = "N/A";
                }
                if (childObject.has("surname")) {
                    surname = childObject.getString("surname");
                } else {
                    surname = "N/A";

                }
                if (childObject.has("email")) {
                    email = childObject.getString("email");
                } else {
                    email = "N/A";
                }
                if (childObject.has("photo")) {
                    photo = childObject.getString("photo");
                } else {
                    photo = "N/A";
                }
                if (childObject.has("role")) {
                    role = childObject.getString("role");
                } else {
                    role = "N/A";
                }
// adds it to an arraylist
                LaunchpadArrayList.add(new EmployeeCustomClass(name, surname, email, role, photo));
//


//

            }

            JSONArray cxpArray = outerObject.getJSONArray("CXP");

            for (int i = 0; i < cxpArray.length(); i++) {

                JSONObject childObject = cxpArray.getJSONObject(i);

                Log.i("OBJECT", "OBJECT........" + childObject.getString("name"));
                Log.i("OBJECT", "OBJECT........" + childObject.getString("surname"));
                Log.i("OBJECT", "OBJECT........" + childObject.getString("email"));
                Log.i("OBJECT", "OBJECT........" + childObject.getString("photo"));
                Log.i("OBJECT", "OBJECT........" + childObject.getString("role"));


                if (childObject.has("name")) {
                    name = childObject.getString("name");
                } else {
                    name = "N/A";
                }
                if (childObject.has("surname")) {
                    surname = childObject.getString("surname");
                } else {
                    surname = "N/A";

                }
                if (childObject.has("email")) {
                    email = childObject.getString("email");
                } else {
                    email = "N/A";
                }

                if (childObject.has("role")) {
                    role = childObject.getString("role");
                } else {
                    role = "N/A";
                }
                if (childObject.has("photo")) {
                    photo = childObject.getString("photo");
                } else {
                    photo = "N/A";
                }

                CXPArrayList.add(new EmployeeCustomClass(name, surname, email, role, photo));


//

            }

            JSONArray mobileArray = outerObject.getJSONArray("Mobile");

            for (int i = 0; i < mobileArray.length(); i++) {

                JSONObject childObject = mobileArray.getJSONObject(i);

                Log.i("OBJECT", "OBJECT........" + childObject.getString("name"));
                Log.i("OBJECT", "OBJECT........" + childObject.getString("surname"));
                Log.i("OBJECT", "OBJECT........" + childObject.getString("email"));
                Log.i("OBJECT", "OBJECT........" + childObject.getString("photo"));
                Log.i("OBJECT", "OBJECT........" + childObject.getString("role"));


                if (childObject.has("name")) {
                    name = childObject.getString("name");
                } else {
                    name = "N/A";
                }
                if (childObject.has("surname")) {
                    surname = childObject.getString("surname");
                } else {
                    surname = "N/A";

                }
                if (childObject.has("email")) {
                    email = childObject.getString("email");
                } else {
                    email = "N/A";
                }
                if (childObject.has("photo")) {
                    photo = childObject.getString("photo");
                } else {
                    photo = "N/A";
                }
                if (childObject.has("role")) {
                    role = childObject.getString("role");
                } else {
                    role = "N/A";
                }

                MobileArrayList.add(new EmployeeCustomClass(name, surname, email, role, photo));


            }
            // adds all to the interface function to be sent back to the list fragment to populate it
            mUpdater.updateAdapter(LaunchpadArrayList, CXPArrayList, MobileArrayList);


        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("ERROR.........", "ERROR FOR ARRAY LIST");

        }


    }

    public interface Adapterhelp {
        void updateAdapter(ArrayList launchpadArrayList, ArrayList cxpArrayList, ArrayList mobileArrayList);
    }
}
