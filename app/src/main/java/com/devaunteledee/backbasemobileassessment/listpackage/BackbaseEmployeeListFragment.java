package com.devaunteledee.backbasemobileassessment.listpackage;

import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.devaunteledee.backbasemobileassessment.UTILS.EmployeeAsyncTask;
import com.devaunteledee.backbasemobileassessment.UTILS.EmployeeCustomClass;
import com.devaunteledee.backbasemobileassessment.UTILS.HeaderClass;
import com.devaunteledee.backbasemobileassessment.UTILS.Item;
import com.devaunteledee.backbasemobileassessment.detail_screen_package.DetailActivity;
import com.devaunteledee.backbasemobileassessment.global.Global;

import java.util.ArrayList;

/**
 * Created by devaunteledee on 3/9/16.
 */
public class BackbaseEmployeeListFragment extends ListFragment implements EmployeeAsyncTask.Adapterhelp {
    public static final String TAG = "BackbaseEmployeeListFragment.TAG";


    ArrayList<Item> listFragmentArrayList = new ArrayList<Item>();
    ArrayList<EmployeeCustomClass> LaunchpadArrayList;
    ArrayList<EmployeeCustomClass> CXPArrayList;
    ArrayList<EmployeeCustomClass> MobileArrayList;

    public static BackbaseEmployeeListFragment newInstance() {

        BackbaseEmployeeListFragment backbaseEmployeeListFragment = new BackbaseEmployeeListFragment();

        return backbaseEmployeeListFragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Network Check
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivityManager != null) {

            NetworkInfo info = connectivityManager.getActiveNetworkInfo();
            // IF connected to the internet this will happen
            if (info != null && info.isConnected()) {


                EmployeeAsyncTask task = new EmployeeAsyncTask(getActivity(), this);

                task.execute("http://nielsmouthaan.nl/backbase/members.php");


            }
//             Else this will show a toast saying to connect to the internet
            else {
                Toast.makeText(getActivity(), "please Connect to a network", Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        BackbaseEmployeeListFragmentAdapter backbaseEmployeeListFragmentAdapter = (BackbaseEmployeeListFragmentAdapter) l.getAdapter();

        EmployeeCustomClass employeeCustomClass = (EmployeeCustomClass) backbaseEmployeeListFragmentAdapter.getItem(position);
/// Putting string extras of the Selected employee into intent to send over to the detail page.
        Intent detailActivityIntent = new Intent(getActivity(), DetailActivity.class);

        detailActivityIntent.putExtra(Global.EMPLOYEE_FIRST_NAME, employeeCustomClass.getName());

        detailActivityIntent.putExtra(Global.EMPLOYEE_LAST_NAME, employeeCustomClass.getSurname());

        detailActivityIntent.putExtra(Global.EMPLOYEE_EMAIL, employeeCustomClass.getEmail());

        detailActivityIntent.putExtra(Global.EMPLOYEE_PHOTO, employeeCustomClass.getPhoto());

        detailActivityIntent.putExtra(Global.EMPLOYEE_ROLE, employeeCustomClass.getRole());

        Log.i("first name: ", employeeCustomClass.getName());

        Log.i("last name: ", employeeCustomClass.getSurname());

        Log.i("Email : ", employeeCustomClass.getEmail());

        Log.i("Photo : ", employeeCustomClass.getPhoto());

        Log.i("Role : ", employeeCustomClass.getRole());

        startActivity(detailActivityIntent);
    }

    @Override
    public void updateAdapter(ArrayList launchpadArrayList, ArrayList cxpArrayList, ArrayList mobileArrayList) {
        LaunchpadArrayList = launchpadArrayList;
        CXPArrayList = cxpArrayList;
        MobileArrayList = mobileArrayList;
// Added first section
        listFragmentArrayList.add(new HeaderClass("Launchpad"));
        // loops through the launchpad array list to get the size and based of the size it will add each inside of the list fragment arraylist to send to the Adapter
        for (int i = 1; i <= LaunchpadArrayList.size(); i++) {
            listFragmentArrayList.add(new EmployeeCustomClass(LaunchpadArrayList.get(i - 1).getName(), LaunchpadArrayList.get(i - 1).getSurname(), LaunchpadArrayList.get(i - 1).getEmail(), LaunchpadArrayList.get(i - 1).getRole(), LaunchpadArrayList.get(i - 1).getPhoto()));
        }
        // Added second section

        listFragmentArrayList.add(new HeaderClass("CXP"));
        for (int i = 1; i <= CXPArrayList.size(); i++) {
            // loops through the CXP array list to get the size and based of the size it will add each inside of the list fragment arraylist to send to the Adapter

            listFragmentArrayList.add(new EmployeeCustomClass(CXPArrayList.get(i - 1).getName(), CXPArrayList.get(i - 1).getSurname(), CXPArrayList.get(i - 1).getEmail(), CXPArrayList.get(i - 1).getRole(), CXPArrayList.get(i - 1).getPhoto()));
        }
        // Added Last section

        listFragmentArrayList.add(new HeaderClass("Mobile"));
        for (int i = 1; i <= MobileArrayList.size(); i++) {
            // loops through the Mobile array list to get the size and based of the size it will add each inside of the list fragment arraylist to send to the Adapter

            listFragmentArrayList.add(new EmployeeCustomClass(MobileArrayList.get(i - 1).getName(), MobileArrayList.get(i - 1).getSurname(), MobileArrayList.get(i - 1).getEmail(), MobileArrayList.get(i - 1).getRole(), MobileArrayList.get(i - 1).getPhoto()));
        }

// Sets list adapter to the BackbaseEmployeeListFragmentAdapter
        BackbaseEmployeeListFragmentAdapter backbaseEmployeeListFragmentAdapter = new BackbaseEmployeeListFragmentAdapter(getActivity(), listFragmentArrayList);

        setListAdapter(backbaseEmployeeListFragmentAdapter);
    }

}
