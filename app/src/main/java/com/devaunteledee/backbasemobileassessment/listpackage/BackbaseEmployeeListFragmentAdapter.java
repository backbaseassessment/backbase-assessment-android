package com.devaunteledee.backbasemobileassessment.listpackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.devaunteledee.backbasemobileassessment.R;
import com.devaunteledee.backbasemobileassessment.UTILS.EmployeeCustomClass;
import com.devaunteledee.backbasemobileassessment.UTILS.HeaderClass;
import com.devaunteledee.backbasemobileassessment.UTILS.Item;

import java.util.ArrayList;

import butterknife.ButterKnife;

/**
 * Created by devaunteledee on 3/10/16.
 */
public class BackbaseEmployeeListFragmentAdapter extends ArrayAdapter<Item> {
    private Context context;
    private ArrayList<Item> itemArrayList;
    private LayoutInflater mInflater;

    public BackbaseEmployeeListFragmentAdapter(Context context, ArrayList<Item> items) {
        super(context, 0, items);
        this.context = context;
        this.itemArrayList = items;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        // Finds the position of either the Employee or the Header
        final Item headerOrEmployee = itemArrayList.get(position);
        // checks if it is null
        if (headerOrEmployee != null) {
            // if it is a section(HEADER)
            if (headerOrEmployee.isSection()) {

                HeaderClass headerClass = (HeaderClass) headerOrEmployee;

                view = mInflater.inflate(R.layout.header_layout, null);
                ButterKnife.bind(this, view);

                view.setOnClickListener(null);
                view.setOnLongClickListener(null);
                view.setLongClickable(false);

                TextView sectionView = ButterKnife.findById(view, R.id.textSeparator);

                sectionView.setText(headerClass.getTitle());

            }
            // Else if it is a Employee
            else {
                EmployeeCustomClass employeeCustomClass = (EmployeeCustomClass) headerOrEmployee;
                // sets the view equal to the list row layout
                view = mInflater.inflate(R.layout.list_row_layout, null);
                ButterKnife.bind(this, view);

                TextView title = ButterKnife.findById(view, R.id.text);
                ImageView imageView = ButterKnife.findById(view, R.id.listImageView);


                title.setText(employeeCustomClass.getName() + " " + employeeCustomClass.getSurname());

                Glide.with(context)
                        .load("http://nielsmouthaan.nl/backbase/photos/" + employeeCustomClass.getPhoto())
                        .placeholder(context.getResources().getDrawable(R.drawable.noimgavailable))
                        .into(imageView);

            }
        }

        return view;
    }


}


