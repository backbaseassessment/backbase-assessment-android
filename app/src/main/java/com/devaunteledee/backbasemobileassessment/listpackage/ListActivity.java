package com.devaunteledee.backbasemobileassessment.listpackage;

import android.app.Activity;
import android.os.Bundle;

import com.devaunteledee.backbasemobileassessment.R;

public class ListActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        // Makes new instance of the Backbase Employee List Fragment
        BackbaseEmployeeListFragment fragment = BackbaseEmployeeListFragment.newInstance();
        // Begins transaction to replace the Layout with the fragment
        getFragmentManager().beginTransaction().replace(R.id.ListContainer, fragment, BackbaseEmployeeListFragment.TAG).commit();

    }

}
