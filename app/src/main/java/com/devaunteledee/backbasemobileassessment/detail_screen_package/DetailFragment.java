package com.devaunteledee.backbasemobileassessment.detail_screen_package;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.devaunteledee.backbasemobileassessment.R;
import com.devaunteledee.backbasemobileassessment.global.Global;

import java.util.Objects;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by devaunteledee on 3/9/16.
 */
public class DetailFragment extends Fragment {
    public static final String TAG = "DetailFragment.TAG";
    String firstName;
    String lastName;
    String email;
    String photo;
    String role;
    @Bind(R.id.EmployeeName)
    TextView nameOfEmployee;
    @Bind(R.id.RoleTextView)
    TextView RoleOfEmployee;
    @Bind(R.id.email)
    TextView emailOfEmployee;
    @Bind(R.id.imageView)
    ImageView employeeImage;

    public static DetailFragment newInstance() {
        DetailFragment detailFragment = new DetailFragment();


        return detailFragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detail_fragment_layout, container, false);
        setHasOptionsMenu(true);

        ButterKnife.bind(this, view);
// grab data from bundle sent from activity
        Bundle bundle = getArguments();

        firstName = bundle.getString(Global.EMPLOYEE_FIRST_NAME);

        lastName = bundle.getString(Global.EMPLOYEE_LAST_NAME);

        email = bundle.getString(Global.EMPLOYEE_EMAIL);


        photo = "http://nielsmouthaan.nl/backbase/photos/" + bundle.getString(Global.EMPLOYEE_PHOTO);

        role = bundle.getString(Global.EMPLOYEE_ROLE);

        nameOfEmployee.setText(firstName + " " + lastName);

        emailOfEmployee.setText("Email: " + "\n" + email);

        RoleOfEmployee.setText("Role: " + "\n" + role);

        Glide.with(getActivity())
                .load(photo)
                .placeholder(getResources().getDrawable(R.drawable.noimgavailable))
                .into(employeeImage);
        return view;


    }

    // ButterKnife on click image view
    @OnClick(R.id.imageView)
    void showPhoto() {
// if there is no image or the image name is JoseCortes a toast will say no valid photo
        if (Objects.equals(photo, "http://nielsmouthaan.nl/backbase/photos/null") || Objects.equals(photo, "http://nielsmouthaan.nl/backbase/photos/JoseCortes.jpg")) {
            Toast.makeText(getActivity(), "No image found", Toast.LENGTH_SHORT).show();
        } else {
            // Intent will take you to the image viewer
            Intent intent = new Intent();
            // Set the action
            intent.setAction(Intent.ACTION_VIEW);
            // set the data being sent over
            intent.setDataAndType(Uri.parse(photo), "image/*");
            // start the activity
            startActivity(intent);

        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_detail, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.email_employee) {

            sendEmail();

        }

        return super.onOptionsItemSelected(item);

    }


    private void sendEmail() {
        // Sets up email intent
        Intent intent = new Intent(Intent.ACTION_SEND);
        // set type for intent
        intent.setType("message/rfc822");
        // put the subject of the email
        intent.putExtra(Intent.EXTRA_SUBJECT, "Backbase Mobile Assessment");
        // put the email address of the employee
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});

        Intent mailer = Intent.createChooser(intent, null);
        startActivity(mailer);
    }


}
