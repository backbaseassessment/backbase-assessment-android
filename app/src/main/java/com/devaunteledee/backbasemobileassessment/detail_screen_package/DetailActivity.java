package com.devaunteledee.backbasemobileassessment.detail_screen_package;

import android.app.Activity;
import android.os.Bundle;

import com.devaunteledee.backbasemobileassessment.R;

public class DetailActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        DetailFragment detailFragment = DetailFragment.newInstance();
        // Sets the data received of the employee to a bundle to send to the detail fragment
        Bundle bundle = getIntent().getExtras();
        detailFragment.setArguments(bundle);

        getFragmentManager().beginTransaction().replace(R.id.detailContainer, detailFragment, DetailFragment.TAG).commit();

    }


}
