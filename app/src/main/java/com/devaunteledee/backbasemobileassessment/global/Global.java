package com.devaunteledee.backbasemobileassessment.global;

/**
 * Created by devaunteledee on 3/11/16.
 */
public class Global {
    // Globally used variables for clean looking code and also no errors using the same string in intents or bundles.


    public static final String EMPLOYEE_FIRST_NAME = "first Name Of Employee";
    public static final String EMPLOYEE_LAST_NAME = "Last Name Of Employee";
    public static final String EMPLOYEE_EMAIL = "Employee's Email";
    public static final String EMPLOYEE_PHOTO = "Employee's Photo";
    public static final String EMPLOYEE_ROLE = "Role OF the employee";


}
